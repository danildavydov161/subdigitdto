package org.example;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> numbers = new ArrayList<>();

        numbers.add("219888888888888888888900130000000000");
        numbers.add("229888888888888888888900130000000000");
        numbers.add("219888888888888888888900330000000000");
        numbers.add("219888888888888888888900430000000000");
        numbers.add("219888888888888888888900530000000000");
        numbers.add("219888888888888888888900630000000000");
        numbers.add("219888888888888888888900730000000000");
        numbers.add("229888888888888888888900830000000000");
        numbers.add("229888888888888888888900930000000000");
        numbers.add("229888888888888888888901030000000000");
        numbers.add("229888888888888888888901130000000000");
        numbers.add("229888888888888888888901230000000000");
        numbers.add("239888888888888888888901330000000000");
        numbers.add("239888888888888888888901430000000000");
        numbers.add("239888888888888888888901530000000000");
        numbers.add("239888888888888888888901630000000000");
        numbers.add("239888888888888888888901730000000000");

        List<SubDigitsDto> SDD = subDigit(numbers);
        SDD.forEach(el -> System.out.println(el));

    }

    public static List<SubDigitsDto> subDigit(List<String> numbers){

        List<SubDigitsDto> list = new ArrayList<>();

        numbers.forEach(el -> {
            SubDigitsDto sdd = new SubDigitsDto();
            sdd.setIku(el.substring(2, 22));
            sdd.setYear("20" + el.substring(0, 2));
            sdd.getSubDigits().add(el.substring(22, 25));
            list.add(sdd);
        });

        Map<String, List<SubDigitsDto>> listByYear = list.stream()
                .collect(Collectors.groupingBy(SubDigitsDto::getYear));

        list.removeAll(list);

        for(Map.Entry<String, List<SubDigitsDto>> item : listByYear.entrySet()){

            SubDigitsDto SDD = new SubDigitsDto();

            for(SubDigitsDto sdd : item.getValue()){
                SDD.setYear(sdd.getYear());
                SDD.setIku(sdd.getIku());
                sdd.getSubDigits().forEach(el -> SDD.getSubDigits().add(el));
            }

            list.add(SDD);
        }

        return list;

    }

}
