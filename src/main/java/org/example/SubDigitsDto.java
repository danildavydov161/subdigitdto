package org.example;

import java.util.*;

class SubDigitsDto {
    private String year;
    private String iku;
    private Set<String> subDigits = new HashSet<>();

    public String getYear(){
        return year;
    }

    public Set<String> getSubDigits(){
        return subDigits;
    }

    public String getIku(){
        return iku;
    }

    public String setYear(String year){
        return this.year = year;
    }

    public String setIku(String iku){
        return this.iku = iku;
    }

    @Override
    public String toString() {
        return "SubDigitsDto{" +
                "year='" + year + '\'' +
                ", iku='" + iku + '\'' +
                ", subDigits=" + subDigits +
                '}';
    }

}
